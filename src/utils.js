const initialState = {
    songs: null,
    player: {
      activeSong: null,
      volume: 100,
      playerState: 'pause'
  }
}

export const get = (url, cb) => fetch(url).then(res => res.json()).then(cb);

export const persistState = (obj) => localStorage.setItem('app-state', JSON.stringify(obj));

export const getState = () => localStorage.getItem('app-state') ? JSON.parse(localStorage.getItem('app-state')) : initialState;