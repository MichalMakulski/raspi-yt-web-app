import React from "react";
import events from '../events';

import './Songs.css'

const SongItem = ({song}) => (
  <li
    className={song.active ? 'song-active' : ''}
    onClick={(e) => events.emit('song-selected', song)} >
    {song.name}
  </li>
)

export default ({songs}) => (
  songs ? <ul>
      {songs.map((song) => <SongItem key={song.id} song={song} />)}
  </ul> : <div>Loading...</div>
)