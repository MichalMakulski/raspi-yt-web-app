import React from "react";
import events from '../events';

import './Player.css'

export default ({player}) => (
  player.activeSong ? <div className="player-container">
    <h4>{player.activeSong.name}</h4>
    <div>
      <button
        onClick={(e) => events.emit('player-state-change', player.playerState)} >
        {player.playerState.toUpperCase()}
      </button>
      <button
        onClick={(e) => events.emit('player-stop')} >
        STOP
      </button>
      <br/>
      <pre>VOLUME</pre>
      <input
        onChange={(e) => events.emit('volume-update', e.target.value)}
        onMouseUp={(e) => events.emit('volume-change', e.target.value)}
        type="range"
        min="0"
        max="100"
        value={player.volume} />
    </div>
  </div> : null
)

