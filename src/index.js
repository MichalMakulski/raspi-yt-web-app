import React from "react";
import ReactDOM from "react-dom";

import Dashboard from './components/Dashboard'

import "./styles.css";

function App() {
  return (
    <div className="App">
      <h1>Raspi YT Player</h1>
      <Dashboard />
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
