import React from "react";
import Songs from '../containers/Songs';
import Player from '../containers/Player';

import events from '../events';

import * as utils from '../utils';
import * as handlers from '../event-handlers/event-handlers';

export default class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = utils.getState()
    this.setState = (function(setState) {
      return function() {
        const result = setState.apply(this, arguments);

        utils.persistState(arguments[0]);

        return result;
      }
    }(this.setState))
  }

  componentDidMount() {
    utils.get('http://192.168.0.20:3001/songs', (json) => {
      this.setState({
        ...this.state,
        songs: json
      });
    });
    events.on('song-selected', handlers.songSelected.bind(this));
    events.on('player-state-change', handlers.playerStateChange.bind(this));
    events.on('player-stop', handlers.playerStop.bind(this));
    events.on('volume-update', handlers.volumeUpdate.bind(this));
    events.on('volume-change', handlers.volumeChange.bind(this));
  }

  render() {
    return <React.Fragment>
      <Songs songs={this.state.songs} />
      <Player player={this.state.player} />
    </React.Fragment>
  }
}