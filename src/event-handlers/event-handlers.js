import * as utils from '../utils';

export function songSelected(selectedSong) {
  utils.get(`http://192.168.0.20:3001/player/load/${selectedSong.id}`, (json) => {
    this.setState({
      player: {
        ...this.state.player,
        activeSong: selectedSong
      },
      songs: this.state.songs.map((song) => ({...song, active: song.id === selectedSong.id}))
    });
  });
}

export function playerStateChange(state) {
  const newState = state === 'pause' ? 'play' : 'pause';

  utils.get(`http://192.168.0.20:3001/player/${state}/${this.state.player.activeSong.id}`, (json) => {
    this.setState({
      ...this.state,
      player: {
        ...this.state.player,
        playerState: newState
      }
    });
  });
}

export function playerStop() {
  utils.get(`http://192.168.0.20:3001/player/stop/${this.state.player.activeSong.id}`, (json) => {
    this.setState({
      player: {
        ...this.state.player,
        activeSong: null,
        playerState: 'pause'
      },
      songs: this.state.songs.map((song) => ({...song, active: false}))
    });
  });
}

export function volumeUpdate(volume) {
  this.setState({
    ...this.state,
    player: {
      ...this.state.player,
      volume
    }
  });
}

export function volumeChange(volume) {
  utils.get(`http://192.168.0.20:3001/player/volume/${volume}`, (json) => {

  });
}